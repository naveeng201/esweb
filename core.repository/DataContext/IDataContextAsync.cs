﻿using System.Threading;
using System.Threading.Tasks;

namespace core.repository.datacontext
{
    public interface IDataContextAsync : IDataContext
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task<int> SaveChangesAsync();
    }
}