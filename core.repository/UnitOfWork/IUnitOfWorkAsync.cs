﻿using System.Threading;
using System.Threading.Tasks;
using core.repository.infrastructure;
using core.repository.repositories;
namespace core.repository.unitofwork
{
    public interface IUnitOfWorkAsync : IUnitOfWork
    {
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        IRepositoryAsync<TEntity> RepositoryAsync<TEntity>() where TEntity : class, IObjectState;
    }
}