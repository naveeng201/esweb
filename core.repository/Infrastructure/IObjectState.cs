﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace core.repository.infrastructure
{
    public interface IObjectState
    {
        [NotMapped]
        ObjectState ObjectState { get; set; }
    }
}