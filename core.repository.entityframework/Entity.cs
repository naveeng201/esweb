﻿using System.ComponentModel.DataAnnotations.Schema;
using core.repository.infrastructure;

namespace core.repository.entityframework
{
    public abstract class Entity : IObjectState
    {
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}