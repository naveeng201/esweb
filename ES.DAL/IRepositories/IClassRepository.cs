﻿using ES.MODELS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ES.DAL.repositories
{
    public interface IClassRepository : IRepository<Class>
    {
    }
}
